import 'package:flutter/material.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;

class SpeechTest extends StatefulWidget {
  final String word;

  SpeechTest({Key key, @required this.word}) : super(key: key);

  @override
  _SpeechTestState createState() => _SpeechTestState();
}

class _SpeechTestState extends State<SpeechTest> {
  String _text;

  void _checkAudioPermission() async {
    final PermissionHandler _permissionHandler = PermissionHandler();
    await _permissionHandler.requestPermissions([PermissionGroup.microphone]);
  }

  @override
  void initState() {
    super.initState();
    _checkAudioPermission();
  }

  void resultListener(SpeechRecognitionResult val) {
    setState(() {
      _text = val.recognizedWords;
    });
  }

  onListenHandler() async {
    stt.SpeechToText speech = stt.SpeechToText();
    bool available = await speech.initialize(onStatus: (_) {}, onError: (_) {});
    if (available) {
      speech.listen(onResult: resultListener, localeId: 'en_US');
    } else {
      print("The user has denied the use of speech recognition.");
    }
    setState(() {
      _text = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Speech Test",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            decoration: BoxDecoration(border: Border.all()),
            padding: EdgeInsets.only(left: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (_text == null) Text(widget.word),
                      if (_text != null &&
                          _text.toLowerCase() == widget.word.toLowerCase())
                        Text(
                          "acerto miserávi",
                          style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      if (_text != null && _text != widget.word)
                        Text(
                          "Errouuuuuuuuu!",
                          style: TextStyle(
                            color: Colors.red,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      if (_text != null) Text(_text),
                    ],
                  ),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.mic),
                    onPressed: onListenHandler,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
