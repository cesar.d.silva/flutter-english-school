import 'dart:math';

import 'package:english_school/display_word.dart';
import 'package:english_school/listening_test.dart';
import 'package:english_school/speech_test.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const phrases = const [
    "May the Force be with you",
    "I'm going to make him an offer he can't refuse",
    "Toto I've got a feeling we're not in Kansas anymore",
    "I love the smell of napalm in the morning",
    "Show me the money",
    "I'll be back",
    "I see dead people",
    "Houston we have a problem",
    "Elementary my dear Watson",
    "You know nothing Jon snow"
  ];

  static get nextWord {
    return phrases[Random().nextInt(phrases.length)];
  }

  String word = nextWord;

  _wordChangeHandler() {
    setState(() {
      word = nextWord;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            DisplayWord(
              refreshWordHandler: _wordChangeHandler,
              word: word,
            ),
            SpeechTest(
              word: word,
            ),
            ListeningTest(
              word: word,
            )
          ],
        ),
      ),
    );
  }
}
