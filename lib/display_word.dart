import 'package:flutter/material.dart';

class DisplayWord extends StatelessWidget {

  final Function refreshWordHandler;
  final String word;

  const DisplayWord({Key key, this.refreshWordHandler, this.word}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(border: Border.all()),
        padding: EdgeInsets.only(left: 10),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(word),
            ),
            Container (
              child: IconButton(
                icon: Icon(Icons.refresh),
                onPressed: refreshWordHandler,
              ),
            )
          ],
        ),
      ),
    );
  }
}
