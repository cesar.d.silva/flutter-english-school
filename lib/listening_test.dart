import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class ListeningTest extends StatefulWidget {
  final String word;
  ListeningTest({Key key, this.word}) : super(key: key);

  @override
  _ListeningTestState createState() => _ListeningTestState();
}

class _ListeningTestState extends State<ListeningTest> {

  FlutterTts flutterTts;
  String language = 'en-US';

  @override
  initState() {
    super.initState();
    initTts();
  }

  Future _getLanguages() async {
    var languages = await flutterTts.getLanguages;
    if (languages != null) setState(() => languages);
  }

  initTts() {
    flutterTts = FlutterTts();
    flutterTts.setSpeechRate(1.0);
    flutterTts.setVolume(1.0);
    flutterTts.setPitch(1.0);

    _getLanguages();

    flutterTts.setStartHandler(() {
      setState(() {
        print("playing");
      });
    });

    flutterTts.setCompletionHandler(() {
      setState(() {
        print("Complete");
      });
    });

    flutterTts.setErrorHandler((msg) {
      setState(() {
        print("error: $msg");
      });
    });
  }

  Future _speak() async {
    flutterTts.setLanguage(language);
    var result = await flutterTts.speak(widget.word);
  }

  onSpeakHandler() {
    _speak();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Listening Test",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            decoration: BoxDecoration(border: Border.all()),
            padding: EdgeInsets.only(left: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(widget.word),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.speaker_phone),
                    onPressed: onSpeakHandler,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    )
    ;
  }
}